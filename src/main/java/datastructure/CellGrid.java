package datastructure;

import cellular.CellState;

import java.util.Arrays;

public class CellGrid implements IGrid {
    int rows;
    int columns;
    CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows = rows;
        this.columns = columns;
        this.grid = new CellState[this.rows][this.columns];

        for(CellState[] row: grid){
            Arrays.fill(row, initialState);
        }
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return this.rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        IGrid grid2 = new CellGrid(numRows(), numColumns(), CellState.DEAD);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                grid2.set(i, j, get(i, j));
            }
        }
        return grid2;
    }
    
}
